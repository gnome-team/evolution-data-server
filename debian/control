Source: evolution-data-server
Section: gnome
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Amin Bandali <bandali@ubuntu.com>, Jeremy Bícha <jbicha@ubuntu.com>, Laurent Bigonville <bigon@debian.org>, Marco Trevisan (Treviño) <marco@ubuntu.com>
Build-Depends: dpkg-dev (>= 1.22.5),
               cmake,
               debhelper-compat (= 13),
               dh-sequence-gir,
               dh-sequence-gnome,
               libtool,
               intltool (>= 0.35.5),
               libcanberra-gtk3-dev (>= 0.25),
               libdb-dev,
               libglib2.0-dev (>= 2.40),
               libglib2.0-doc (>= 2.40),
               libgirepository1.0-dev (>= 1.59.1),
               libsecret-1-dev (>= 0.5),
               libgoa-1.0-dev (>= 3.8),
               libgtk-3-dev (>= 3.10),
               libgtk-4-dev (>= 4.4),
               libgweather-4-dev (>= 4.1),
               libical-dev (>= 3.0.7),
               libicu-dev,
               libjson-glib-dev (>= 1.0.4) [!ia64],
               libkrb5-dev,
               libldap2-dev,
               liboauth-dev (>= 0.9.4),
               librest-dev (>= 0.7),
               libnss3-dev,
               libnspr4-dev,
               libsoup-3.0-dev (>= 3.1.1),
               libsqlite3-dev (>= 3.7.17),
               libwebkit2gtk-4.1-dev [!ia64],
               libwebkitgtk-6.0-dev [!ia64],
               libxml2-dev (>= 2.0.0),
               gtk-doc-tools (>= 1.14),
               gperf,
               pkgconf,
               valac (>= 0.22),
               libphonenumber-dev [!hppa !hurd-any !ia64],
               db-util <!nocheck>,
               dbus <!nocheck>,
Rules-Requires-Root: binary-targets
Standards-Version: 4.7.0
Homepage: https://gitlab.gnome.org/GNOME/evolution/-/wikis/home
Vcs-Git: https://salsa.debian.org/gnome-team/evolution-data-server.git
Vcs-Browser: https://salsa.debian.org/gnome-team/evolution-data-server

Package: evolution-data-server
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         evolution-data-server-common (>= ${source:Version}),
         libcamel-1.2-64t64 (= ${binary:Version}),
         gnome-keyring,
Suggests: evolution
Description: evolution database backend server
 The data server, called "Evolution Data Server" is responsible for managing
 mail, calendar, addressbook, tasks and memo information.

Package: evolution-data-server-common
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: architecture independent files for Evolution Data Server
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains the architecture independent files needed
 by the evolution-data-server package.

Package: evolution-data-server-tests
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         evolution-common,
         evolution-data-server
Description: Installed tests for the evolution database backend server
 The data server, called "Evolution Data Server" is responsible for managing
 mail, calendar, addressbook, tasks and memo information.
 .
 This package contains test programs, designed to be run as part of a
 regression testsuite.

Package: evolution-data-server-dev
Section: devel
Architecture: any
Depends: ${misc:Depends},
         libglib2.0-dev (>= 2.40),
         libnss3-dev,
         libnspr4-dev
Provides: ${gir:Provides}
Description: Development files for evolution-data-server (metapackage)
 This package contains header files and static library of evolution-data-server.

Package: evolution-data-server-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: devhelp
Description: Documentation files for the Evolution Data Server libraries
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains the API documentation for all the individual
 Evolution Data Server libraries.

Package: libedataserver-1.2-27t64
Provides: ${t64:Provides}
Replaces: libedataserver-1.2-27
Breaks: libedataserver-1.2-27 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends},
         evolution-data-server-common (>= ${gnome:Version})
Description: Utility library for evolution data servers
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package is a utility library for evolution-data-server.

Package: libedataserver1.2-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libedataserver-1.2-27t64 (= ${binary:Version}),
         gir1.2-edataserver-1.2 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends},
         libcamel1.2-dev (= ${binary:Version}),
         libxml2-dev,
         libglib2.0-dev (>= 2.40),
         libjson-glib-dev (>= 1.0.4) [!ia64],
         libsoup-3.0-dev (>= 3.1.1),
         libsecret-1-dev
Provides: ${gir:Provides}
Description: Utility library for evolution data servers (development files)
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains header files and static library for libedataserver.

Package: gir1.2-edataserver-1.2
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: GObject introspection for the EDataServer library
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains introspection data for the libedataserver
 library. It can be used by packages using the GIRepository format to
 generate dynamic bindings.

Package: libedataserverui-1.2-4t64
Provides: ${t64:Provides}
Replaces: libedataserverui-1.2-4
Breaks: libedataserverui-1.2-4 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends},
         evolution-data-server-common (>= ${gnome:Version})
Description: Utility library for evolution data servers
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package is a UI utility library for evolution-data-server.

Package: libedataserverui1.2-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libedataserverui-1.2-4t64 (= ${binary:Version}),
         gir1.2-edataserverui-1.2 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends},
         libedata-book1.2-dev,
         libcamel1.2-dev (= ${binary:Version}),
         libecal2.0-dev (= ${binary:Version}),
         libedataserver1.2-dev (= ${binary:Version}),
         libxml2-dev,
         libglib2.0-dev (>= 2.40),
         libgtk-3-dev,
         libsoup-3.0-dev (>= 3.1.1),
         libsecret-1-dev
Provides: ${gir:Provides}
Description: Utility library for evolution data servers (development files)
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains development files for libedataserverui.

Package: gir1.2-edataserverui-1.2
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: GObject introspection for the EDataServerUI library
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains introspection data for the libedataserverui
 library. It can be used by packages using the GIRepository format to
 generate dynamic bindings.

Package: libedataserverui4-1.0-0t64
Provides: ${t64:Provides}
Replaces: libedataserverui4-1.0-0
Breaks: libedataserverui4-1.0-0 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends},
         evolution-data-server-common (>= ${gnome:Version})
Description: GTK4 utility library for evolution data servers
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package is a GTK4 UI utility library for evolution-data-server.

Package: libedataserverui4-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libedataserverui4-1.0-0t64 (= ${binary:Version}),
         gir1.2-edataserverui4-1.0 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends},
         libedata-book1.2-dev,
         libcamel1.2-dev (= ${binary:Version}),
         libecal2.0-dev (= ${binary:Version}),
         libedataserver1.2-dev (= ${binary:Version}),
         libxml2-dev,
         libglib2.0-dev (>= 2.40),
         libgtk-4-dev (>= 4.4),
         libsoup-3.0-dev (>= 3.1.1),
         libsecret-1-dev
Provides: ${gir:Provides}
Description: GTK4 Utility library for evolution data server (development files)
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains development files for libedataserverui.

Package: gir1.2-edataserverui4-1.0
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: GObject introspection for the EDataServerUI4 library
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains introspection data for the libedataserverui4
 library. It can be used by packages using the GIRepository format to
 generate dynamic bindings.

Package: libcamel-1.2-64t64
Provides: ${t64:Provides}
Replaces: libcamel-1.2-64
Breaks: libcamel-1.2-64 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Evolution MIME message handling library
 Camel is a generic messaging library. It supports the standard
 messaging system for receiving and sending messages. It is the
 messaging backend for Evolution.
 .
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.

Package: libcamel1.2-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libcamel-1.2-64t64 (= ${binary:Version}),
         gir1.2-camel-1.2 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         libglib2.0-dev (>= 2.40),
         libsqlite3-dev (>= 3.5),
         libnss3-dev,
         libnspr4-dev
Provides: ${gir:Provides}
Description: Development files for libcamel
 This package contains header files and static library for libcamel.
 .
 Camel is a generic messaging library. It supports the standard
 messaging system for receiving and sending messages. It is the
 messaging backend for Evolution.
 .
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.

Package: gir1.2-camel-1.2
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: GObject introspection for the Camel library
 Camel is a generic messaging library. It supports the standard
 messaging system for receiving and sending messages. It is the
 messaging backend for Evolution.
 .
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.
 .
 This package contains introspection data for the libcamel
 library. It can be used by packages using the GIRepository format to
 generate dynamic bindings.

Package: libebook-1.2-21t64
Provides: ${t64:Provides}
Replaces: libebook-1.2-21
Breaks: libebook-1.2-21 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Client library for evolution address books
 This package is a client library for evolution addressbooks.
 .
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.

Package: libebook1.2-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libebook-1.2-21t64 (= ${binary:Version}),
         gir1.2-ebook-1.2 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         libedataserver1.2-dev (= ${binary:Version}),
         libebook-contacts1.2-dev (= ${binary:Version}),
         libcamel1.2-dev (= ${binary:Version}),
         libedata-book1.2-dev (= ${binary:Version}),
         libglib2.0-dev (>= 2.40),
Provides: ${gir:Provides}
Description: Client library for evolution address books (development files)
 This package contains header files and static library for libebook.
 .
 libebook is a client library for evolution addressbooks.
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.

Package: gir1.2-ebook-1.2
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: GObject introspection for the EBook library
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains introspection data for the libeebook
 library. It can be used by packages using the GIRepository format to
 generate dynamic bindings.

Package: libedata-book-1.2-27t64
Provides: ${t64:Provides}
Replaces: libedata-book-1.2-27
Breaks: libedata-book-1.2-27 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Backend library for evolution address books
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.
 .
 This package is a backend library for evolution address book.

Package: libedata-book1.2-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libedata-book-1.2-27t64 (= ${binary:Version}),
         gir1.2-edatabook-1.2 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         libebook-contacts1.2-dev (= ${binary:Version}),
         libebackend1.2-dev (= ${binary:Version}),
         libglib2.0-dev (>= 2.40),
Provides: ${gir:Provides}
Description: Backend library for evolution address books (development files)
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.
 .
 This package contains header files and static library for libedata-book.

Package: gir1.2-edatabook-1.2
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: GObject introspection for the EBook library
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains introspection data for the libedataserver
 library. It can be used by packages using the GIRepository format to
 generate dynamic bindings.

Package: gir1.2-ebookcontacts-1.2
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: GObject introspection for the EBook Contacts library
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains introspection data for the libeebook
 library. It can be used by packages using the GIRepository format to
 generate dynamic bindings.

Package: libebook-contacts-1.2-4t64
Provides: ${t64:Provides}
Replaces: libebook-contacts-1.2-4
Breaks: libebook-contacts-1.2-4 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Client library for evolution contacts books
 This package is a client library for evolution addressbooks.
 .
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.

Package: libebook-contacts1.2-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libebook-contacts-1.2-4t64 (= ${binary:Version}),
         gir1.2-ebookcontacts-1.2 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         libedataserver1.2-dev (= ${binary:Version}),
         libcamel1.2-dev (= ${binary:Version})
Provides: ${gir:Provides}
Description: Client library for evolution contacts books (development files)
 This package contains header files and static library for libebook.
 .
 libebook is a client library for evolution addressbooks.
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.

Package: libecal-2.0-3
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Client library for evolution calendars
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.
 .
 This package is a client library for evolution calendar.

Package: libecal2.0-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libecal-2.0-3 (= ${binary:Version}),
         gir1.2-ecal-2.0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         libical-dev (>= 2.0),
         libedataserver1.2-dev (= ${binary:Version}),
         libglib2.0-dev (>= 2.40),
Provides: ${gir:Provides}
Description: Client library for evolution calendars (development files)
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.
 .
 This package contains header files and static library for libecal.

Package: gir1.2-ecal-2.0
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: GObject introspection for the ECal library
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains introspection data for the libedataserver
 library. It can be used by packages using the GIRepository format to
 generate dynamic bindings.

Package: libedata-cal-2.0-2t64
Provides: ${t64:Provides}
Replaces: libedata-cal-2.0-2
Breaks: libedata-cal-2.0-2 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Backend library for evolution calendars
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.
 .
 This package is a backend library for evolution calendar.

Package: libedata-cal2.0-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libedata-cal-2.0-2t64 (= ${binary:Version}),
         gir1.2-edatacal-2.0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         libical-dev (>= 2.0),
         libecal2.0-dev (= ${binary:Version}),
         libebackend1.2-dev (= ${binary:Version}),
         libglib2.0-dev (>= 2.40),
Provides: ${gir:Provides}
Description: Backend library for evolution calendars (development files)
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.
 .
 This package contains header files and static library for libedata-cal.

Package: gir1.2-edatacal-2.0
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: GObject introspection for the EDataCal library
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains introspection data for the libedata-cal
 library. It can be used by packages using the GIRepository format to
 generate dynamic bindings.

Package: libebackend-1.2-11t64
Provides: ${t64:Provides}
Replaces: libebackend-1.2-11
Breaks: libebackend-1.2-11 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Utility library for evolution data servers
 This package is a utility library for evolution-data-servers providing
 backend functions to access data.
 .
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.

Package: libebackend1.2-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libebackend-1.2-11t64 (= ${binary:Version}),
         gir1.2-ebackend-1.2 (= ${binary:Version}),
         ${misc:Depends},
         libedataserver1.2-dev (= ${binary:Version}),
         libglib2.0-dev (>= 2.40),
Provides: ${gir:Provides}
Description: Utility library for evolution data servers (development files)
 This package contains header files and static library for libebackend.
 .
 libebackend is a utility library for evolution-data-server providing
 backend function to access data.
 .
 Evolution is the integrated mail, calendar, task and address book
 distributed suite from Novell, Inc.

Package: gir1.2-ebackend-1.2
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: GObject introspection for the EBackend library
 The data server, called "Evolution Data Server" is responsible for managing
 calendar and addressbook information.
 .
 This package contains introspection data for the libedataserver
 library. It can be used by packages using the GIRepository format to
 generate dynamic bindings.
